require 'pry'

require_relative '../lib/pivot_index_finder'

begin
  numbers = ARGV.map { |param| Integer(param) }
  result = PivotIndexFinder.execute(numbers)

  puts "Pivot index for #{numbers} is #{result}"
rescue ArgumentError => e
  raise "Invalid input. Please make sure all params are an integer."
end
