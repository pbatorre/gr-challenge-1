require 'pivot_index_finder'

describe PivotIndexFinder do
  describe '.execute' do
    subject(:pivot_index) do
      PivotIndexFinder.execute([1, 4, 6, 3, 2])
    end

    it 'returns the pivot index' do
      expect(pivot_index).to eq 2
    end
  end
end
