# Setup

Run `bundle install` to install the dependencies

# Testing

To run the example provided in the problem, run the following command:

`ruby bin/runner.rb 1 4 6 3 2`

**Output**: `Pivot index for [1, 4, 6, 3, 2] is 2`
