class PivotIndexFinder
  NOT_FOUND = -1
  MINIMUM_LENGTH = 3

  def self.execute(array)
    return NOT_FOUND if invalid?(array)

    left_cursor = 0
    right_cursor = array.length - 1
    pivot_range = []

    while (right_cursor - 1) > left_cursor
      left_sum = array[0..left_cursor].sum
      right_sum = array[right_cursor..array.length - 1].sum

      if left_sum == right_sum
        pivot_range = [left_cursor + 1, right_cursor - 1]
      end

      right_cursor -= 1 if left_sum >= right_sum
      left_cursor += 1 if left_sum <= right_sum
    end

    pivot_range.any? ? pivot_range.first : NOT_FOUND
  end

  def self.invalid?(array)
    !array.is_a?(Array) || array.length < MINIMUM_LENGTH
  end
end
